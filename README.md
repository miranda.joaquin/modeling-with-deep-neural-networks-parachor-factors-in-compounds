# Modeling with deep neural networks Parachor factors in compounds

I used a neural network to determine the value of the Parachor coefficient in 227 pure chemical compounds. For a given compound, the neural network can discriminate the group contribution. The code uses TensorFlow and Sklearn.
